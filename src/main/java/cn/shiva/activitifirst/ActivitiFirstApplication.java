package cn.shiva.activitifirst;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivitiFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivitiFirstApplication.class, args);
    }

}
